<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Penerimaan UI -Buat Account</title>
</head>
<body>
	<h2>Login</h2>
	<table>
		<tr>
			<td></td>
			<td style="color: red; display: none; font-size: small;" id="td_username" >Field Ini Harus Diisi.</td>
		<tr>
			<td>Username</td>
			<td><input type="text" id="username"/></td>
			
		<tr/>
		<tr>
			<td></td>
			<td style="color: red; display: none; font-size: small;" id="td_password" >Field Ini Harus Diisi.</td>
		<tr>
		<tr>
			<td>Password </td>
			<td>  <input type="password" id="password"/>   </td>
		<tr/>
		<tr>
			<td></td>
			<td style="color: red; display: none; font-size: small;" id="td_repassword" >Field Ini Harus Diisi.</td>
		<tr>
		<tr>
			<td>Ulangi Password</td>
			<td>  <input type="password" id="repassword" onchange="ceksama()"/>   </td>
		<tr/>
	</table>
	<br/>
	<h2>Identitas</h2>
	<table>
		<tr>
			<td></td>
			<td style="color: red; display: none; font-size: small;" id="td_nama_identitas" >Field Ini Harus Diisi.</td>
		<tr>
		<tr>
			<td>Nama Sesuai Identitas</td>
			<td><input type="text" id="nama_identitas"/></td>
		<tr/>
		<tr>
			<td></td>
			<td style="font-size: Small; color: grey;">Username yang Anda inginkan (hanya dapat terdiri dari huruf, angka, dan _). Username tidak boleh mengandung spasi.</td>
		</tr>
		<tr>
			<td></td>
			<td style="color: red; display: none" id="td_nama_ijazah" >Field Ini Harus Diisi.</td>
		<tr>
		<tr>
			<td>Nama Sesuai Ijazah </td>
			<td>  <input type="text" id="nama_ijazah"/></td>
			<td style="color: red; display: none" id="td_nama_ijazah" >Field Ini Harus Diisi.</td>
		<tr/>
		<tr>
			<td>Pilih Jenis Kelamin</td>
			<td>
				<select>
					<option value="-">Pilih Jenis Identitas</option>
					<option value="KTP">KTP</option>
					<option value="SIM">SIM</option>
					<option value="Paspor">Paspor</option>
					<option value="Kartu_pelajar">Kartu Pelajar (Berfoto)</option>
				</select>
			</td>
		</tr>
		<tr>
			<td></td>
			<td style="color: red; display: none; font-size: small;" id="td_nomor_identitas" >Field Ini Harus Diisi Ya.</td>
		<tr>
		<tr>
			<td>Nomor Identitas </td>
			<td><input type="text" id="nomor_identitas" onchange="ceksama()"/></td>
		<tr/>
		<tr>
			<td>Kewarganegaraan</td>
			<td>
				<select>
					<option value="indonesia">Indonesia</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Jenis Kelamin</td>
			<td><input type="radio" name="jk"> Pria</td> 
		</tr>
		<tr>
			<td></td>
			<td><input type="radio" name="jk"> Wanita</td>
		</tr>
	</table>
	<br></br>
	<h2>Kontak</h2>
	<table>
		<tr>
			<td></td>
			<td style="color: red; display: none" id="td_alamat" >Field ini belum diisi.</td>		<tr>
		<tr>
			<td>Alamat Tetap</td>
			<td><input type="text" id="alamat"/></td></tr>
		</tr>
		<tr>
			<td>Negara</td>
			<td>
				<select id="negara" onchange="addKota();">
					<option value="-">--Pilih--</option>
					<option value="idn">Indonesia</option>
					<option value="malaysia">Malaysia</option>
					<option value="vietnam">Vietnam</option>
					<option value="rusia">Rusia</option>
					<option value="amerika">Amerika Serikat</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Propinsi</td>
			<td>
				<select id="kota" onchange="addKab();">
					<option value="-">--Pilih--</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Kabupaten/Kotamadya</td>
			<td>
				<select id="kabupaten">
				<option value="-">--Pilih--</option>
				</select>
			</td>
		</tr>
		<tr>
			<td></td>
			<td style="color: red; display: none" id="td_alamat_sekarang" >Field ini belum diisi.</td>		<tr>
		<tr>
		<tr>
			<td>Alamat Saat Ini</td>
			<td><input type="text" id="alamat_sekarang"/></td></tr>
		</tr>
		<tr>
			<td></td>
			<td style="color: red; display: none" id="td_nomor_telepon" >Field ini belum diisi.</td>		<tr>
		<tr>
		<tr>
			<td>Nomor Telepon</td>
			<td><input type="text" id="nomor_telepon"/></td></tr>
		</tr>
		<tr>
			<td></td>
			<td style="color: red; display: none" id="td_nomor_hp" >Field ini belum diisi.</td>		<tr>
		<tr>
		<tr>
			<td>Nomor HP</td>
			<td><input type="text" id="nomor_hp"/></td></tr>
		</tr>
		<tr>
			<td></td>
			<td style="color: red; display: none" id="td_email" >Field ini belum diisi.</td>		<tr>
		<tr>
		<tr>
			<td>Email</td>
			<td><input type="text" id="email"/></td></tr>
			<td style="color: red; display: none" id="td_email" >nama belum diisi.</td>
		</tr>
		<tr>
			<td></td>
			<td style="color: red; display: none" id="td_reemail" >Field ini belum diisi.</td>		<tr>
		<tr>
		<tr>
			<td></td>
			<td style="color: red; display: none" id="td_rereemail" >Email tidak sama.</td>		<tr>
		<tr>
		<tr>
			<td>Ulangi Email</td>
			<td><input type="text" id="re_email" onchange="ceksama();"/></td></tr>
		</tr>
		<tr>
			<td></td>
			<td><button type="button" onclick="cekisi()"/> Simpan</button></td>
		</tr>
		
	</table>

</body>
<script type="text/javascript">
function cekisi() {
		var username = document.getElementById("username");
		var td_username = document.getElementById("td_username");
		if (username.value=="") {
			td_username.style.display="block";
			username.style.borderColor="red";
		} else {
			td_username.style.display="none";
			username.style.borderColor="blue";
		}
		
		var password = document.getElementById("password");
		var td_password = document.getElementById("td_password");
		if (password.value=="") {
			td_password.style.display="block";
			password.style.borderColor="red";
		} else {
			td_password.style.display="none";
			password.style.borderColor="blue";
		}
		
		var repassword = document.getElementById("repassword");
		var td_repassword = document.getElementById("td_repassword");
		if (repassword.value=="") {
			td_repassword.style.display="block";
			repassword.style.borderColor="red";
		} else {
			td_repassword.style.display="none";
			repassword.style.borderColor="blue";
		}
		
		var nama_identitas = document.getElementById("nama_identitas");
		var td_nama_identitas = document.getElementById("td_nama_identitas");
		if (nama_identitas.value=="") {
			td_nama_identitas.style.display="block";
			nama_identitas.style.borderColor="red";
		} else {
			td_nama_identitas.style.display="none";
			nama_identitas.style.borderColor="blue";
		}
		
		var nama_ijazah = document.getElementById("nama_ijazah");
		var td_nama_ijazah = document.getElementById("td_nama_ijazah");
		if (nama_ijazah.value=="") {
			td_nama_ijazah.style.display="block";
			nama_ijazah.style.borderColor="red";
		} else {
			td_nama_ijazah.style.display="none";
			nama_ijazah.style.borderColor="blue";
		}
		
		var nomor_identitas = document.getElementById("nomor_identitas");
		var td_nomor_identitas = document.getElementById("td_nomor_identitas");
		if (nomor_identitas.value=="") {
			td_nomor_identitas.style.display="block";
			nomor_identitas.style.borderColor="red";
		} else {
			td_nomor_identitas.style.display="none";
			nomor_identitas.style.borderColor="blue";
		}
		
		var alamat = document.getElementById("alamat");
		var td_alamat = document.getElementById("td_alamat");
		if (alamat.value=="") {
			td_alamat.style.display="block";
			alamat.style.borderColor="red";
		} else {
			td_alamat.style.display="none";
			alamat.style.borderColor="blue";
		}
		
		var alamat_sekarang = document.getElementById("alamat_sekarang");
		var td_alamat_sekarang = document.getElementById("td_alamat_sekarang");
		if (alamat_sekarang.value=="") {
			td_alamat_sekarang.style.display="block";
			alamat_sekarang.style.borderColor="red";
		} else {
			td_alamat_sekarang.style.display="none";
			alamat_sekarang.style.borderColor="blue";
		}
		
		var nomor_telepon = document.getElementById("nomor_telepon");
		var td_nomor_telepon = document.getElementById("td_nomor_telepon");
		if (nomor_telepon.value=="") {
			td_nomor_telepon.style.display="block";
			nomor_telepon.style.borderColor="red";
		} else {
			td_nomor_telepon.style.display="none";
			nomor_telepon.style.borderColor="blue";
		}
		
		var nomor_hp= document.getElementById("nomor_hp");
		var td_nomor_hp = document.getElementById("td_nomor_hp");
		if (nomor_hp.value=="") {
			td_nomor_hp.style.display="block";
			nomor_hp.style.borderColor="red";
		} else {
			td_nomor_hp.style.display="none";
			nomor_hp.style.borderColor="blue";
		}
		
		var email= document.getElementById("email");
		var td_email = document.getElementById("td_email");
		if (email.value=="") {
			td_email.style.display="block";
			email.style.borderColor="red";
		} else {
			td_email.style.display="none";
			email.style.borderColor="blue";
		}
		
		var reemail= document.getElementById("reemail");
		var td_reemail = document.getElementById("td_reemail");
		if (reemail.value=="") {
			td_reemail.style.display="block";
			reemail.style.borderColor="red";
		} else {
			td_reemail.style.display="none";
			reemail.style.borderColor="blue";
		}
	}
function ceksama() {
		var email = document.getElementById("email");
		var reemail = document.getElementById("reemail");
		var td_rereemail = document.getElementById("td_rereemail");
		if (email.value!=reemail.value) {
			td_reeemail.style.display="block";
		} else {
			td_rereemail.style.display="none";
		}
	}
function addKota(){
		var negara= document.getElementById("negara").value;
		var kota= document.getElementById("kota");
		
		var option1 = document.createElement("option");
		var option2 = document.createElement("option");
		var option3 = document.createElement("option");
		var option4 = document.createElement("option");
		
		if(negara=="idn"){
			option1.text="Jawa Timur";	option1.value="jatim";
			option2.text="DKI Jakarta"; option2.value="jakarta";
			option3.text="DI Yogyakarta"; option3.value="jogja";
			
			kota.add(option1);
			kota.add(option2);
			kota.add(option3);
		}
		else {
			option4.text="Lain-lain";
			
			kota.add(option4);
		}
	}
function addKab(){
		var kota= document.getElementById("kota").value;
		var kab= document.getElementById("kabupaten");
		
		var option1 = document.createElement("option");
		var option2 = document.createElement("option");
		var option3 = document.createElement("option");
		var option4 = document.createElement("option");
		var option5 = document.createElement("option");
		var option6 = document.createElement("option");
		var option7 = document.createElement("option");
		var option8 = document.createElement("option");
		
		if(kota=="jatim"){
			option1.text="Surabaya";
			option2.text="Malang";
			option3.text="Gresik";
			
			kab.add(option1);
			kab.add(option2);
			kab.add(option3);
		}
		else if(kota=="jakarta"){
			option1.text="Jakarta Barat";
			option2.text="Jakarta Selatan";
			option3.text="Jakarta Utara";
			option4.text="Jakarta Timur";
			
			kab.add(option1);
			kab.add(option2);
			kab.add(option3);
			kab.add(option4);
		}
		else if(kota=="jogja"){
			option1.text="Sleman";
			option2.text="Bantul";
			option3.text="Wonosari";
			
			kab.add(option1);
			kab.add(option2);
			kab.add(option3);
		}
	}

</script>
</html>
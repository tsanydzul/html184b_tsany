<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Beasiswa LPDP - Create Account</title>
</head>
<body>
<form action="#">
	<table>
		<tr>
			<td><h1>Daftar Akun</h1></td>
		</tr>
		<tr></tr>
		<tr>
			<td><i type="text" style="color: red; font-size: small; ">Catatan : semua isian bertanda * perlu diisi.</i></td>
		</tr>
		<tr>
			<td><h2>Informasi Akun</h2></td>
		</tr>
		<tr>
			<td>Email* </td>
			<td><input type="text" id="email"/></td>
		<tr/>
		<tr>
			<td>Konfirmasi Email*</td>
			<td>  <input type="text" id="reemail" onchange="ceksama()"/>   </td>
		<tr/>
		<tr>
			<td></td>
			<td><i type="text" style="color: red; font-size: small; ">*Pastikan email anda adalah email pribadi dan aktif</i></td>
		<tr/>
		<tr>
			<td>Password*</td>
			<td><input type="password" id="password"/></td>
		<tr/>
			<td></td>
			<td><i type="text" style="color: red; font-size: X-small; ">*Password terdiri dari 8-20 karakter, minimal satu huruf kecil, minimal satu huruf besar, minimal satu angka dan minimal satu karakter spesial.<br> Karakter spesial yang dapat digunakan: ! , @ , # , $ atau %</br></i></td>
		<tr>
			<td>Konfirmasi Pssword*</td>
			<td>  <input type="password" id="repassword" onchange="ceksama()"/>   </td>
		<tr/>
			
		<tr>
			<td><h2>Informasi Pribadi</h2></td>
		</tr>
		<tr>
			<td>Nama Lengkap</td>
			<td><input type="text" id="alamat"/></td></tr>
		</tr>
		<tr>
			<td>Nama Panggilan Tetap</td>
			<td><input type="text" id="alamat"/></td></tr>
		</tr>
		<tr>
			<td>Nomor KTP</td>
			<td><input type="text" id="alamat"/></td></tr>
		</tr>
		<tr>
			<td>Jenis Kelamin</td>
			<td>
				<select id="jk">
					<option value="laki">Laki-laki</option>
					<option value="perempuan">Perempuan</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Status Menikah</td>
			<td>
				<select id="sm">
					<option value="belum">Belum Menikah</option>
					<option value="menikah">Menikah</option>
					<option value="cerai">Janda/Duda/Cerai</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Agama</td>
			<td>
				<select id="ag">
					<option value="islam">Islam</option>
					<option value="protestan">Protestan</option>
					<option value="katolik">Katolik</option>
					<option value="hindu">Hindu</option>
					<option value="buddha">Buddha</option>
					<option value="konghucu">Kong Hu Cu</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Golongan Darah</td>
			<td>
				<select id="sm">
					<option value="A">A</option>
					<option value="B">B</option>
					<option value="AB">AB</option>
					<option value="O">O</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Tempat Lahir</td>
			<td><input type="text" id="alamat"/></td></tr>
		</tr>
		<tr>
			<td>Tanggal Lahir</td>
			<td><input type="text" id="alamat"/></td></tr>
		</tr>
		<tr>
			<td><i style="color: red">Sesuai KTP</i></td>
		</tr>
		<tr>
			<td>Alamat</td>
			<td><input type="text" id="alamat"/></td></tr>
		</tr>
		<tr>
			<td><i style="color: red">Sesuai KTP</i></td>
		</tr>
		<tr>
			<td>Kabupaten/Kota</td>
			<td>
				<select id="sm">
					<option value="A">Aceh</option>
					<option value="B">DKI Jakarta</option>
					<option value="AB">Jawa Timur</option>
					<option value="O">Jawa Tengah</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Kode Pos</td>
			<td><input type="text" id="alamat"/></td></tr>
		</tr>
		<tr>
			<td>Hanphone*</td>
			<td><input type="text" id="alamat"/></td></tr>
		</tr>
		<tr>
			<td><i style="color: red; font-size: small" >Harap diisi dengan<br>format menggunakan kode negara.</br><br>Untuk Indonesia, gantikan angka 0</br><br>di paling depan dengan angka 62.</br><br>Contoh: 0811111111</br><br>mohon diisi dengan 62811111111.</br></i></td>
		</tr>
		<tr>
			<td>Telepon</td>
			<td><input type="text" id="alamat"/></td></tr>
		</tr>
		<tr>
			<td>Jenis Pekerjaan</td>
			<td>
				<select id="jp">
					<option value="A">Ahli Profesi</option>
					<option value="B">Bidan/Perawat</option>
					<option value="AB">Dokter</option>
					<option value="O">Dosen</option>
					<option value="AB">Fresh Graduate</option>
					<option value="O">Guru</option>
					<option value="O">Hakim</option>
					<option value="AB">Jaksa</option>
					<option value="O">Lainnya</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Foto 3x4</td>
			<td><input type="file"></tr>
		</tr>
		<tr></tr>
		<tr>
			<td><h2>Pendidikan Terakhir</h2></td>
		</tr>
		<tr>
			<td>Jenjang Penidikan Terakhir</td>
			<td>
				<select id="jpend">
					<option value="S1">S1/D4</option>
					<option value="S2">S2</option>
					<option value="S3">S3</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Tahun Kelulusan</td>
			<td>
				<select id="jpend">
					<option value="S1">1980</option>
					<option value="S2">1981</option>
					<option value="S3">1982</option>
					<option value="S1">1983</option>
					<option value="S1">1984</option>
					<option value="S1">1985</option>
					<option value="S1">1986</option>
					<option value="S1">1987</option>
					<option value="S1">1988</option>
					<option value="S1">1989</option>
					<option value="S1">1990</option>
					<option value="S1">1991</option>
					<option value="S1">1992</option>
					<option value="S1">1993</option>
					<option value="S1">1994</option>
					<option value="S1">1995</option>
					<option value="S1">1996</option>
					<option value="S1">1997</option>
					<option value="S1">1998</option>
					<option value="S1">1999</option>
					<option value="S1">2000</option>
					<option value="S1">2001</option>
					<option value="S1">2002</option>
					<option value="S1">2003</option>
					<option value="S1">2004</option>
					<option value="S1">2005</option>
					<option value="S1">2006</option>
					<option value="S1">2007</option>
					<option value="S1">2008</option>
					<option value="S1">2009</option>
					<option value="S1">2010</option>
					<option value="S1">2011</option>
					<option value="S1">2012</option>
					<option value="S1">2013</option>
					<option value="S1">2014</option>
					<option value="S1">2015</option>
					<option value="S1">2016</option>
					<option value="S1">2017</option>
					<option value="S1">2018</option>
					<option value="S1">2019</option>
					<option value="S1">2020</option>
					<option value="S1">2021</option>	
				</select>
			</td>
		</tr>
		<tr>
			<td>Asal Universitasr</td>
			<td>
				<select id="asaluniv">
					<option value="dn">Dalam Negeri</option>
					<option value="ln">Luar Negeri</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Bahasa Pengantar*</td>
			<td>
				<select id="bahasa">
					<option value="dn">Inggris</option>
					<option value="ln">Rusia</option>
					<option value="ln">Mandarin</option>
					<option value="ln">Arab</option>
					<option value="ln">Perancis</option>
					<option value="ln">Spanyol</option>
					<option value="ln">Lainnya</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Prodi*</td>
			<td>
				<select id="bahasa">
					<option value="dn">Administrasi</option>
					<option value="ln">Agribisnis</option>
					<option value="ln">Administrasi Hotel</option>
					<option value="ln">Agroteknologi</option>
					<option value="ln">Asuransi Kesehatan</option>
					<option value="ln">Biosain</option>
					<option value="ln">Lainnya</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>IPK Terakhir dengan<br> Format 4.00*</br></td>
			<td><input type="text" id="alamat"/></td></tr>
		</tr>
		<tr>
			<td></td>
			<td><a href="http://www.foreigncredits.com/Resources/GPA-Calculator">Link Convert IPK luar Negeri</a></td>
		<tr/>
		<tr>
			<td><button type="button"onclick="cekisi()" style="align-self: middle;" /> Submit</button></td>
		</tr>
	</table>

</form>	
	
</body>
<script type="text/javascript">
	function cekisi() {
		var password = document.getElementById("password");
		var td_password = document.getElementById("td_password");
		if (password.value=="") {
			td_password.style.display="block";
			password.style.borderColor="red";
		} else {
			td_password.style.display="none";
			password.style.borderColor="blue";
		}
		
		var repassword = document.getElementById("repassword");
		var td_repassword = document.getElementById("td_repassword");
		if (password.value=="") {
			td_repassword.style.display="block";
			repassword.style.borderColor="red";
		} else {
			td_repassword.style.display="none";
			repassword.style.borderColor="blue";
		}
	}
	function ceksama() {
		var password = document.getElementById("password");
		var repassword = document.getElementById("repassword");
		var td_repassword = document.getElementById("td_repassword");
		if (password.value!=repassword.value) {
			td_repassword.style.display="block";
		} else {
			td_repassword.style.display="none";
		}
		
		var email = document.getElementById("email");
		var reemail = document.getElementById("reemail");
		var td_reemail = document.getElementById("td_reemail");
		if (email.value!=reemail.value) {
			td_reemail.style.display="block";
		} else {
			td_reemail.style.display="none";
		}
	}
</script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Formulir Penmaru</title>
</head>
<body>
<h1>Formulir Pendaftaran UMY</h1>
<form action="#">
<table>
		<tr>
			<td></td>
		</tr>
		<tr></tr>
		<tr>
			<td>Nama:</td>
			<td><input type="text" id="nama"/></td>
			<td style="color: red; display: none" id="td_nama" >Please fill out this field </td>
		<tr/>
		<tr>
			<td>No KTP/NIK:</td>
			<td><input type="text" id="id"/></td>
			<td style="color: red; display: none" id="td_id" >Please fill out this field </td>
		<tr/>
		<tr>
			<td>Jenis Kelamin:</td>
			<td><input type="radio" name="jk"> Pria</td> 
		</tr>
		<tr>
			<td></td>
			<td><input type="radio" name="jk"> Wanita</td>
		</tr>
		<tr>
			<td>Agama:</td>
			<td><input type="radio" name="ag">Islam</td> 
		</tr>
		<tr>
			<td></td>
			<td><input type="radio" name="ag">Kristen</td>
		</tr>
		<tr>
			<td></td>
			<td><input type="radio" name="ag">Katolik</td>
		</tr>
		<tr>
			<td></td>
			<td><input type="radio" name="ag">Hindu</td>
		</tr>
		<tr>
			<td></td>
			<td><input type="radio" name="ag">Buddha</td>
		</tr>
		<tr>
			<td>Tanggal Lahir</td>
			<td><input type="text" id="tl"/></td>
		</tr>
		<tr>
			<td>Tempat Lahir</td>
			<td><input type="text" id="ttl"/></td>
		</tr>
		<tr>
			<td>Negara Lahir</td>
			<td>
				<select id="negara" onchange="addKota();">
					<option value="-">--Pilih--</option>
					<option value="idn">Indonesia</option>
					<option value="malaysia">Malaysia</option>
					<option value="vietnam">Vietnam</option>
					<option value="rusia">Rusia</option>
					<option value="amerika">Amerika Serikat</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Propinsi Lahir</td>
			<td>
				<select id="kota" onchange="addKab();">
					<option value="-">--Pilih--</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Kabupaten Lahir</td>
			<td>
				<select id="kabupaten">
				<option value="-">--Pilih--</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Alamat Asal</td>
			<td><input type="text" id="alamatasal"/></td>
		</tr>
				<tr>
			<td>Negara Asal</td>
			<td>
				<select id="negaraasal" onchange="addKotaasal();">
					<option value="-">--Pilih--</option>
					<option value="idn">Indonesia</option>
					<option value="malaysia">Malaysia</option>
					<option value="vietnam">Vietnam</option>
					<option value="rusia">Rusia</option>
					<option value="amerika">Amerika Serikat</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Propinsi Asal</td>
			<td>
				<select id="kotaasal" onchange="addKabasal();">
					<option value="-">--Pilih--</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Kabupaten Asal</td>
			<td>
				<select id="kabupatenasal" onchange="addKecasal();">
				<option value="-">--Pilih--</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Kecamatan Asal</td>
			<td>
				<select id="kecamatanasal">
				<option value="-">--Pilih--</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Kelurahan Asal</td>
			<td><input type="text" id="kelurahanasal"/></td>
		</tr>
		<tr>
			<td>Kode Pos Asal</td>
			<td><input type="text" id="kodepos"/></td>
		</tr>
		<tr>
			<td>Email</td>
			<td><input type="text" id="email"/></td>
		</tr>
		<tr>
			<td>No Hanphone</td>
			<td><input type="text" id="nomorhp"/></td>
		</tr>
		<tr>
			<td>IPA/IPS/IPC</td>
			<td>
				<select id="jurusan">
					<option value="-">IPA/IPS/IPC</option>
					<option value="ipa">Kelompok IPA</option>
					<option value="ips">Kelompok IPS</option>
					<option value="ipc">Kelompok IPC</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Harga</td>
			<td>
				<select id="harga">
					<option value="-">4000000</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Prodi Pilihan 1:</td>
			<td>
				<select id="prodi">
					<option value="-">-</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Kelas Program Pilihan 1:</td>
			<td>
				<select id="kelas">
					<option value="-">Reguler</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Prodi Pilihan 2:</td>
			<td>
				<select id="prodi2">
					<option value="-">-</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Kelas Program Pilihan 2:</td>
			<td>
				<select id="kelas2">
					<option value="-">Reguler</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Prodi Pilihan 1:</td>
			<td>
				<select id="prodi">
					<option value="-">-</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Negara Asal SLTA:</td>
			<td>
				<select id="negaraslta" onchange="addKotaasal1();">
					<option value="-">--Pilih--</option>
					<option value="idn">Indonesia</option>
					<option value="malaysia">Malaysia</option>
					<option value="vietnam">Vietnam</option>
					<option value="rusia">Rusia</option>
					<option value="amerika">Amerika Serikat</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Provinsi Asal SLTA:</td>
			<td>
				<select id="kotaslta" onchange="addKabasal1();">
					<option value="-">--Pilih--</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Kabupaten Asal</td>
			<td>
				<select id="kabupatenslta" onchange="addsma();">
				<option value="-">--Pilih--</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Nama Asal SLTA:</td>
			<td>
				<select id="asal_slta" >
					<option value="-"></option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Alamat Asal SLTA:</td>
			<td>
				<select id="alamat_slta">
					<option value="-">Alamat</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Jurusan SLTA</td>
			<td>
				<select id="jurusan_asal">
					<option value="-">Jurusan SLTA</option>
					<option value="ipa">[Tidak Ada]</option>
					<option value="ipa">AGAMA</option>
					<option value="ips">BAHASA</option>
					<option value="ipa">IPA</option>
					<option value="ips">IPS</option>
					<option value="ipc">KEJURUAN</option>
				</select>
			</td>
		</tr>
		<tr>
			<td></td>
			<td><button type="button" onclick="cekisi();">Simpan</button></td>
		</tr>
</table>
</form>
</body>
<script type="text/javascript">
function addKota(){
		var negara= document.getElementById("negara").value;
		var kota= document.getElementById("kota");
		
		var option1 = document.createElement("option");
		var option2 = document.createElement("option");
		var option3 = document.createElement("option");
		var option4 = document.createElement("option");
		
		if(negara=="idn"){
			option1.text="Jawa Timur";	option1.value="jatim";
			option2.text="DKI Jakarta"; option2.value="jakarta";
			option3.text="DI Yogyakarta"; option3.value="jogja";
			
			kota.add(option1);
			kota.add(option2);
			kota.add(option3);
		}
		else {
			option4.text="Lain-lain";
			
			kota.add(option4);
		}
	}
function addKab(){
		var kota= document.getElementById("kota").value;
		var kab= document.getElementById("kabupaten");
		
		var option1 = document.createElement("option");
		var option2 = document.createElement("option");
		var option3 = document.createElement("option");
		var option4 = document.createElement("option");
		var option5 = document.createElement("option");
		var option6 = document.createElement("option");
		var option7 = document.createElement("option");
		var option8 = document.createElement("option");
		
		if(kota=="jatim"){
			option1.text="Surabaya";
			option2.text="Malang";
			option3.text="Gresik";
			
			kab.add(option1);
			kab.add(option2);
			kab.add(option3);
		}
		else if(kota=="jakarta"){
			option1.text="Jakarta Barat";
			option2.text="Jakarta Selatan";
			option3.text="Jakarta Utara";
			option4.text="Jakarta Timur";
			
			kab.add(option1);
			kab.add(option2);
			kab.add(option3);
			kab.add(option4);
		}
		else if(kota=="jogja"){
			option1.text="Sleman";
			option2.text="Bantul";
			option3.text="Wonosari";
			
			kab.add(option1);
			kab.add(option2);
			kab.add(option3);
		}
	}

function addKotaasal(){
	var negara= document.getElementById("negaraasal").value;
	var kota= document.getElementById("kotaasal");
	
	var option1 = document.createElement("option");
	var option2 = document.createElement("option");
	var option3 = document.createElement("option");
	var option4 = document.createElement("option");
	
	if(negara=="idn"){
		option1.text="Jawa Timur";	option1.value="jatim";
		option2.text="DKI Jakarta"; option2.value="jakarta";
		option3.text="DI Yogyakarta"; option3.value="jogja";
		
		kota.add(option1);
		kota.add(option2);
		kota.add(option3);
	}
	else {
		option4.text="Lain-lain";
		
		kota.add(option4);
	}
}
function addKabasal(){
	var kota= document.getElementById("kotaasal").value;
	var kab= document.getElementById("kabupatenasal");
	
	var option1 = document.createElement("option");
	var option2 = document.createElement("option");
	var option3 = document.createElement("option");
	var option4 = document.createElement("option");
	var option5 = document.createElement("option");
	var option6 = document.createElement("option");
	var option7 = document.createElement("option");
	var option8 = document.createElement("option");
	
	if(kota=="jatim"){
		option1.text="Surabaya"; option1.value="sby";
		option2.text="Malang";	option1.value="mlng";
		option3.text="Gresik";	option1.value="gsk";
		
		kab.add(option1);
		kab.add(option2);
		kab.add(option3);
	}
	else if(kota=="jakarta"){
		option1.text="Jakarta Barat";	option1.value="jakbar";
		option2.text="Jakarta Selatan";	option2.value="jaksel";
		option3.text="Jakarta Utara";	option3.value="jakut";
		option4.text="Jakarta Timur";	option4.value="jaktim";
		
		kab.add(option1);
		kab.add(option2);
		kab.add(option3);
		kab.add(option4);
	}
	else if(kota=="jogja"){
		option1.text="Sleman";	option1.value="sleman";
		option2.text="Bantul";	option1.value="bntl";
		option3.text="Gunung Kidul";	option1.value="gk";
		
		kab.add(option1);
		kab.add(option2);
		kab.add(option3);
	}
}
function addKecasal(){
	var kab= document.getElementById("kabupatenasal").value;
	var kec= document.getElementById("kecamatanasal");
	
	var option1 = document.createElement("option");
	var option2 = document.createElement("option");
	var option3 = document.createElement("option");
	var option4 = document.createElement("option");
	var option5 = document.createElement("option");
	var option6 = document.createElement("option");
	var option7 = document.createElement("option");
	var option8 = document.createElement("option");
	
	if(kab=="jaktim"){
		option1.text="Cipayung";
		option2.text="Cakung";
		option3.text="Cicaras";
		
		kec.add(option1);
		kec.add(option2);
		kec.add(option3);
	}
	else if(kab=="jakbar"){
		option1.text="Cengkareng";
		option2.text="Taman Sari";
		option3.text="Tambora";
		option4.text="Kebon Jeruk";
		
		kec.add(option1);
		kec.add(option2);
		kec.add(option3);
		kec.add(option4);
	}
	else if(kab=="jakut"){
		option1.text="Cilincing";
		option2.text="Kelapa Gading";
		option3.text="Pademangan";
		
		kec.add(option1);
		kec.add(option2);
		kec.add(option3);
	}
	else if(kab=="jaksel"){
		option1.text="Cilandak";
		option2.text="Kebayoran Lama";
		option3.text="Jagakarsa";
		
		kec.add(option1);
		kec.add(option2);
		kec.add(option3);
	}
	else if(kab=="sby"){
		option1.text="Benowo";
		option2.text="Bubutan";
		option3.text="Gubeng";
		
		kec.add(option1);
		kec.add(option2);
		kec.add(option3);
	}
	else if(kab=="mlng"){
		option1.text="Blimbing";
		option2.text="Polowijen";
		option3.text="Klojen";
		
		kec.add(option1);
		kec.add(option2);
		kec.add(option3);
	}
	else if(kab=="gsk"){
		option1.text="Benjeng";
		option2.text="Panggang";
		option3.text="Bungah";
		
		kec.add(option1);
		kec.add(option2);
		kec.add(option3);
	}
	else if(kab=="sleman"){
		option1.text="Depok";
		option2.text="Kalasan";
		option3.text="Mlati";
		
		kec.add(option1);
		kec.add(option2);
		kec.add(option3);
	}
	else if(kab=="bntl"){
		option1.text="Banguntapan";
		option2.text="Pleret";
		option3.text="Jetis";
		
		kec.add(option1);
		kec.add(option2);
		kec.add(option3);
	}
	else if(kab=="gk"){
		option1.text="Karangmojo";
		option2.text="Ponjong";
		option3.text="Playen";
		
		kec.add(option1);
		kec.add(option2);
		kec.add(option3);
	}
}
function addprodi() {
	var jurusan= document.getElementById("jurusan").value;
	var prodi1= document.getElementById("prodi1");
	
	
	var option1 = document.createElement("option");
	var option2 = document.createElement("option");
	var option3 = document.createElement("option");
	var option4 = document.createElement("option");
	var option5 = document.createElement("option");
	var option6 = document.createElement("option");
	
	if (jurusan==ipa){
		option1.text="S1 Teknik Elektro";
		option2.text="S1 Teknik Mesin";
		option3.text="S1 Teknik Informatika";
		
		prodi1.add(option1);
		prodi1.add(option2);
		prodi1.add(option3);
	}
	else if (jurusan==ips){
		option1.text="S1 Manajemen";
		option2.text="S1 Sejarah";
		option3.text="S1 Hukum";
		
		prodi1.add(option1);
		prodi1.add(option2);
		prodi1.add(option3);
	}
	else if (jurusan==ips){
		option1.text="S1 Manajemen";
		option2.text="S1 Sejarah";
		option3.text="S1 Hukum";
		option4.text="S1 Teknik Elektro";
		option5.text="S1 Teknik Mesin";
		option6.text="S1 Teknik Informatika";
		
		prodi1.add(option1);
		prodi1.add(option2);
		prodi1.add(option3);
		prodi1.add(option1);
		prodi1.add(option2);
		prodi1.add(option3);
	}
}
function addprodi2() {
	var jurusan= document.getElementById("jurusan").value;
	var prodi2= document.getElementById("prodi1");
	
	
	var option1 = document.createElement("option");
	var option2 = document.createElement("option");
	var option3 = document.createElement("option");
	var option4 = document.createElement("option");
	var option5 = document.createElement("option");
	var option6 = document.createElement("option");
	
	if (jurusan==ipa){
		option1.text="S1 Teknik Elektro";
		option2.text="S1 Teknik Mesin";
		option3.text="S1 Teknik Informatika";
		
		prodi2.add(option1);
		prodi2.add(option2);
		prodi2.add(option3);
	}
	else if (jurusan==ips){
		option1.text="S1 Manajemen";
		option2.text="S1 Sejarah";
		option3.text="S1 Hukum";
		
		prodi2.add(option1);
		prodi2.add(option2);
		prodi2.add(option3);
	}
	else if (jurusan==ips){
		option1.text="S1 Manajemen";
		option2.text="S1 Sejarah";
		option3.text="S1 Hukum";
		option4.text="S1 Teknik Elektro";
		option5.text="S1 Teknik Mesin";
		option6.text="S1 Teknik Informatika";
		
		prodi2.add(option1);
		prodi2.add(option2);
		prodi2.add(option3);
		prodi2.add(option1);
		prodi2.add(option2);
		prodi2.add(option3);
	}
}
function addKotaasal1(){
	var negara= document.getElementById("negaraslta").value;
	var kota= document.getElementById("kotaslta");
	
	var option1 = document.createElement("option");
	var option2 = document.createElement("option");
	var option3 = document.createElement("option");
	var option4 = document.createElement("option");
	
	if(negara=="idn"){
		option1.text="Jawa Timur";	option1.value="jatim";
		option2.text="DKI Jakarta"; option2.value="jakarta";
		option3.text="DI Yogyakarta"; option3.value="jogja";
		
		kota.add(option1);
		kota.add(option2);
		kota.add(option3);
	}
	else {
		option4.text="Lain-lain";
		
		kota.add(option4);
	}
}
function addKabasal1(){
	var kota= document.getElementById("kotaslta").value;
	var kab= document.getElementById("kabupatenslta");
	
	var option1 = document.createElement("option");
	var option2 = document.createElement("option");
	var option3 = document.createElement("option");
	var option4 = document.createElement("option");
	var option5 = document.createElement("option");
	var option6 = document.createElement("option");
	var option7 = document.createElement("option");
	var option8 = document.createElement("option");
	
	if(kota=="jatim"){
		option1.text="Surabaya"; option1.value="sby";
		option2.text="Malang";	option1.value="mlng";
		option3.text="Gresik";	option1.value="gsk";
		
		kab.add(option1);
		kab.add(option2);
		kab.add(option3);
	}
	else if(kota=="jakarta"){
		option1.text="Jakarta Barat";	option1.value="jakbar";
		option2.text="Jakarta Selatan";	option2.value="jaksel";
		option3.text="Jakarta Utara";	option3.value="jakut";
		option4.text="Jakarta Timur";	option4.value="jaktim";
		
		kab.add(option1);
		kab.add(option2);
		kab.add(option3);
		kab.add(option4);
	}
	else if(kota=="jogja"){
		option1.text="Sleman";	option1.value="sleman";
		option2.text="Bantul";	option1.value="bntl";
		option3.text="Gunung Kidul";	option1.value="gk";
		
		kab.add(option1);
		kab.add(option2);
		kab.add(option3);
	}
}
function addsma(){
	var sma= document.getElementById("asal_slta");
	var kab= document.getElementById("kabupatenslta").value;
	
	var option1 = document.createElement("option");
	var option2 = document.createElement("option");
	var option3 = document.createElement("option");
	var option4 = document.createElement("option");
	var option5 = document.createElement("option");
	var option6 = document.createElement("option");
	var option7 = document.createElement("option");
	var option8 = document.createElement("option");
	
	if(kab=="sby"){
		option1.text="SMA SBY"; option1.value="sby";
		option2.text="SMK SBY";	option1.value="mlng";
		option3.text="MA SBY";	option1.value="gsk";
		
		sma.add(option1);
		sma.add(option2);
		sma.add(option3);
	}
	else if(kab=="mlng"){
		option1.text="SMA MALANG";	option1.value="jakbar";
		option2.text="SMK MALANG";	option2.value="jaksel";
		option3.text="MA MALANG";	option3.value="jakut";
		
		sma.add(option1);
		sma.add(option2);
		sma.add(option3);
		sma.add(option4);
	}
	else if(kab=="gsk"){
		option1.text="SMA GRESIK";	option1.value="jakbar";
		option2.text="SMK GRESIK";	option2.value="jaksel";
		option3.text="MA GRESIK";	option3.value="jakut";
		
		sma.add(option1);
		sma.add(option2);
		sma.add(option3);
	}
	else if(kab=="jakbar"){
		option1.text="SMA JAKBAR";	option1.value="jakbar";
		option2.text="SMK JAKBAR";	option2.value="jaksel";
		option3.text="MA JAKBAR";	option3.value="jakut";
		
		sma.add(option1);
		sma.add(option2);
		sma.add(option3);
	}
	else if(kab=="jaktim"){
		option1.text="SMA JAKTIM";	option1.value="jakbar";
		option2.text="SMK JAKTIM";	option2.value="jaksel";
		option3.text="MA JAKTIM";	option3.value="jakut";
		
		sma.add(option1);
		sma.add(option2);
		sma.add(option3);
	}
	else if(kab=="jakut"){
		option1.text="SMA JAKUT";	option1.value="jakbar";
		option2.text="SMK JAKUT";	option2.value="jaksel";
		option3.text="MA JAKUT";	option3.value="jakut";
		
		sma.add(option1);
		sma.add(option2);
		sma.add(option3);
	}
	else if(kab=="jaksel"){
		option1.text="SMA JAKSEL";	option1.value="jakbar";
		option2.text="SMK JAKSEL";	option2.value="jaksel";
		option3.text="MA JAKSEL";	option3.value="jakut";
		
		sma.add(option1);
		sma.add(option2);
		sma.add(option3);
	}
	else if(kab=="sleman"){
		option1.text="SMA SLEMAN";	option1.value="jakbar";
		option2.text="SMK SLEMAN";	option2.value="jaksel";
		option3.text="MA SLEMAN";	option3.value="jakut";
		
		sma.add(option1);
		sma.add(option2);
		sma.add(option3);
	}
	else if(kab=="bntl"){
		option1.text="SMA BANTUL";	option1.value="jakbar";
		option2.text="SMK BANTUL";	option2.value="jaksel";
		option3.text="MA BANTUL";	option3.value="jakut";
		
		sma.add(option1);
		sma.add(option2);
		sma.add(option3);
	}
	else if(kab=="gk"){
		option1.text="SMA GUNUNG KIDUL";	option1.value="jakbar";
		option2.text="SMK GUNUNG KIDUL";	option2.value="jaksel";
		option3.text="MA GUNUNG KIDUL";	option3.value="jakut";
		
		sma.add(option1);
		sma.add(option2);
		sma.add(option3);
	}
}
</script>
</html>
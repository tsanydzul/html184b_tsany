<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Judul Atas</title>
</head>
<body>
	Tsany
	Enter <br/> br <br/> <br/> ok
	<br/>
	<h1>h1</h1>
	<h2>h2</h2>
	<h3>h3</h3>
	<h4>h4</h4>
	<h5>h5</h5>
	<h6>h6</h6>
	<a style="color: red; font-family: fantasy;font-size: x-large;">Custom</a>
	<br/>
	<b>bold</b> <i>italic</i> <u>underline</u>
	<br/>
	<table border="1">
		<tr >
			<td style="color: red;background-color: yellow;">A</td>
			<td>B</td>
		</tr>
		<tr style="color: red;background-color: red;">
			<td>C</td>
			<td>D</td>
		</tr>	
	</table>
	<br/>
	<table border="1" >
		<tr>
			<td>A</td>
			<td>B</td>
		</tr>
		<tr>
			<td>C</td>
			<td>D</td>
		</tr>	
	</table>
	<br/>
	<table border="0" >
		<tr>
			<td>A</td>
			<td>B</td>
		</tr>
		<tr>
			<td>C</td>
			<td>D</td>
		</tr>	
	</table>
	<br/>
	Form
	<br/>
	<form action="#">
		txt<input type="text"/> <br/>
		txt size <input type="text" size="5"><br/>
		txt maxlength <input type="text" maxlength="5"><br/>
		txt place holder <input type="text" placeholder="bayang2"><br/>
		txt style <input type="text" style="color: red;background-color: yellow;"><br/>
		password <input type="password"><br/>
		file <input type="file"> <br/>
		hidden <input type="hidden"> <br/>
		radio <input type="radio"> Satu <input type="radio"> <br/>
		radio <input type="radio" name="sama"> Satu <input type="radio" name="sama">Dua <br/>
		
		select <select>
				<option>Satu</option>
				<option>dua</option>
				<option>tiga</option>
			</select>
		<br/>
		
		<a href="a"> link</a> <br/>
		img <img alt="" src="Contoh.jpg">
		
	</form>
	
</body>
</html>